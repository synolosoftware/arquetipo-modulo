package com.synolo.nombre.ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.synolo.nombre.business.GreeterEJBLocal;
import com.synolo.nombre.business.GreeterException;
import com.synolo.nombre.dao.GenericoDAO;

/**
 * @author bmaxwell Session Bean implementation class GreeterEJB
 */
@Stateless
public class GreeterEJB implements GreeterEJBLocal {

	@Inject
	private GenericoDAO genericoDAO;

	/**
	 * Default constructor.
	 */
	public GreeterEJB() {
	}

	public String sayHello(String name) throws GreeterException {

		if (name == null || name.equals(""))
			throw new GreeterException("name cannot be null or empty");

		return "Hello " + name;
	}

	@Override
	public <T> List<T> list(Class<T> clazz) {
		return genericoDAO.list(clazz);
	}

}
