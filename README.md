Usage

Calling mvn archetype:create-from-project the plugin first resolves the package by guessing the project directory.

It then generates the directory tree of the archetype in the target/generated-sources/archetype directory.
