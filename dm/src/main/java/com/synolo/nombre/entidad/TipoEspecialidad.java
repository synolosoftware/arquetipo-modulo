package com.synolo.nombre.entidad;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * Copyright (c) 2015, Synolo Software S.A.S. All rights reserved.
 * 
 * Entidad de acceso a datos del catalogo de los tipos de especialidad del
 * sistema
 * 
 * @author Enar Gomez
 * @version 1.0 fecha de Creacion 31/10/2015
 * 
 */
@Entity
public class TipoEspecialidad {

	private Long id;
	private String codigo;
	private String descripcion;

	@Id
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
