package com.synolo.nombre.dao;

import java.util.List;

public interface GenericoDAO {

	<T> List<T> list(Class<?> clazz);

	<T> T merge(T arg0);

	void persist(Object arg0);

	void remove(Object arg0);

	<T> List<T> ListForParent(Class<?> clazz, String parentAtribute, Object parent);

}
