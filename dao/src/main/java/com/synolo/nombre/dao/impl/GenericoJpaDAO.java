package com.synolo.nombre.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.synolo.nombre.dao.GenericoDAO;

public class GenericoJpaDAO implements GenericoDAO {

	@PersistenceContext()
	private EntityManager em;

	@Override
	public <T> List<T> list(Class<?> clazz) {
		// requerimiento VPM-11657,se agrega where
		// "tz.activo=1"
		final String jpql = "SELECT tz FROM " + clazz.getSimpleName() + " tz";
		final Query query = em.createQuery(jpql);
		return query.getResultList();
	}

	@Override
	public <T> T merge(T arg0) {
		return em.merge(arg0);
	}

	@Override
	public void persist(Object arg0) {
		em.persist(arg0);
	}

	@Override
	public void remove(Object arg0) {
		em.remove(arg0);
	}

	@Override
	public <T> List<T> ListForParent(Class<?> clazz, String parentAtribute,
			Object parent) {
		final String jpql = "SELECT tz FROM " + clazz.getSimpleName()
				+ " tz where tz." + parentAtribute + "=:parent";
		final Query query = em.createQuery(jpql).setParameter("parent", parent);
		return query.getResultList();
	}

}
